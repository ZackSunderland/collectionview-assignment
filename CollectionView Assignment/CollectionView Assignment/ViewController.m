//
//  ViewController.m
//  CollectionView Assignment
//
//  Created by Zack Sunderland on 3/1/16.
//  Copyright (c) 2016 ___FULLUSERNAME___. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

typedef void(^myCompletion)(NSArray* data);

@implementation ViewController

- (void)viewDidLoad	
{
    [super viewDidLoad];
    
    
    UICollectionViewFlowLayout *layout=[[UICollectionViewFlowLayout alloc] init];
    cView=[[UICollectionView alloc] initWithFrame:self.view.frame collectionViewLayout:layout];
    [cView setDataSource:self];
    [cView setDelegate:self];
    
    [cView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"cellIdentifier"];
    [cView setBackgroundColor:[UIColor redColor]];
    
    [self.view addSubview:cView];
    
    
    arrayOfSevenWonders = [NSMutableArray new];
    
    [self getSevenWondersInfoWithResponseBlock:^(NSArray *array) {
        
        for (NSDictionary* d in array) {
            wonderOfTheWorld* wotwo = [wonderOfTheWorld new];
            wotwo.image = d[@"image"];
            wotwo.image_thumb = d[@"image_thumb"];
            
            [arrayOfSevenWonders addObject:wotwo];
            
            
            NSURL *url = [NSURL URLWithString:wotwo.image_thumb];
            
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
            
            
            NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
            
            [[session dataTaskWithRequest:request
                        completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                            
                            dispatch_async(dispatch_get_main_queue(), ^{
                                //Your main thread code goes in here
                                self.imageview.image = [UIImage imageWithData:data];
                            });
                            
                            
                            
                        }] resume];
        }
        
        
        
        
    }];
    
}


-(void)getSevenWondersInfoWithResponseBlock:(myCompletion) completion {
    NSString *urlString = [NSString stringWithFormat: @"http://aasquaredapps.com/Class/sevenwonders.json"];
    
    NSURL *url = [NSURL URLWithString:urlString];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    
    [[session dataTaskWithRequest:request
                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                    
                    // do stuff
                    completion([NSJSONSerialization JSONObjectWithData:data options:0 error:&error]);
                    
                }] resume];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 15;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"cellIdentifier" forIndexPath:indexPath];
    
    cell.backgroundColor=[UIColor greenColor];
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(self.view.bounds.size.width/2 - 5, 250);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
