//
//  wonderOfTheWorld.h
//  CollectionView Assignment
//
//  Created by Zack Sunderland on 3/1/16.
//  Copyright (c) 2016 Zack Sunderland. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface wonderOfTheWorld : NSObject
@property (nonatomic, strong) NSString* image;
@property (nonatomic, strong) NSString* image_thumb;
@property (nonatomic, strong) NSString* location;
@property (nonatomic, strong) NSString* name;
@property (nonatomic, strong) NSString* region;
@property (nonatomic, strong) NSString* wikipedia;
@property (nonatomic, strong) NSString* year_built;

@property (nonatomic, assign) CLLocationCoordinate2D coord;

@end
