//
//  ViewController.h
//  CollectionView Assignment
//
//  Created by Zack Sunderland on 3/1/16.
//  Copyright (c) 2016 ___FULLUSERNAME___. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "wonderOfTheWorld.h"

@interface ViewController : UIViewController <UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout>
{
    NSMutableArray* arrayOfSevenWonders;

    UICollectionView* cView;
}
@property (weak, nonatomic) IBOutlet UIImageView *imageview;

@end
