//
//  AppDelegate.h
//  CollectionView Assignment
//
//  Created by Zack Sunderland on 3/1/16.
//  Copyright (c) 2016 ___FULLUSERNAME___. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
